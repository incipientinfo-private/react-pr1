
## Coding challenge

1. Use [react-bootstrap](https://react-bootstrap.github.io/getting-started/introduction) for design. our main concern will be functionality as well as code quality.
2. Your task is, create a user module for basic CRUD operations. You can see [demo](http://php.demo4work.com/dev6/react_pr1/users) with credentials (email : admin@admin.com, password: password)
 
      a. Get all users - create route : /users to show all users list 
                        Also formate activate as shown in [users page](http://php.demo4work.com/dev6/react_pr1/users) . You can skip pagination (server side) and search functionality if you want.   
                        b. Create User - create route : /users/create  [Reference](http://php.demo4work.com/dev6/react_pr1/users/create)  
      c. View & Update User - create route : /users/update/:uuid 
      d. Delete User

  ``` 
  Further required description for these sub tasks is given below in required APIs section.
  Bonus Points: pagination & searching in users table is optional but if you will perform then it will be added as bonus points. 
  ```

3. First of all you need to login with admin account credentials (email : admin@admin.com, password: password). It will return bearer token. 
4. With that bearer token you can call further APIs for user module.

## Project Setup:
Please clone this project and run `npm install`. Ready for `npm start`.

## Required APIs   

NOTE: You can also import workspace from [here](http://php.demo4work.com/dev6/Practical_APIs.postman_collection.json) to postman.
 

1. **Login** :  It has already been implemented.   
`POST : http://php.demo4work.com/dev6/practical_api/api/auth/login`   
  Required Fields   :   
  i) email  
  ii) password 
  
  Send Data

 
```
{
  "email" : "admin@admin.com",
  "password" : "password"
}
```

Response
```
{
 "status": 1,
  "message": "Successfully logged in",
  "message_debug": null,
  "record_count": 1,
  "data": {
    "uuid": "e17c9807-08f5-4f6f-b278-c9d9cde9cc8e",
    "name": "Admin",
    "surname": "Admin",
    "username": "admin",
    "email": "admin@admin.com",
    "phone_number": "1234567890",
    "phone_country_code": "+91",
    "company": "XYZ",
    "image": null,
    "image_url": null,
    "role": "Admin",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9waHAuZGVtbzR3b3JrLmNvbVwvZGV2NlwvcHJhY3RpY2FsX2FwaVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU5ODUwNTEzOSwiZXhwIjoxNjA3MTQ1MTM5LCJuYmYiOjE1OTg1MDUxMzksImp0aSI6IkZOZ1V5a3RQMW53NngxVGUiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.YG-HpbTzlFt4swdjfoaeGbUMmdOq1PLcKUuOd1WxOL0",
    "token_type": "bearer",
    "expires_in": 144000,
    "expires_at": 1607145139
  }
}
```

## Header required for below API

```
{
  Authorization : "Bearer access_token"
}
```

2. **Get Users**:   
a. This will return records with pagination (server side) based on parameters send.   
b. You can skip pagination if you want.   
c. To skip pagination, do not send any parameters in API(except header). 

  `POST : http://php.demo4work.com/mts/backend_api/api/user/filter`

Send Data : All below fields are optional.
```
{
	"pageSize" : "1",
	"pageNumber" : "5",
	"sortField" : "",
	"sortOrder" : "",
	"filter" : {
          "name" : "",
          "surname" : "",
          "username" : "",
          "email" : "",
          "company" : "",
          "phone_number" : "",
          "activate" : "0/1"
        }
}
```

Response of API :

```
{
  "status": 1,
  "message": "success",
  "message_debug": null,
  "record_count": 1,
  "data": {
    "draw": 0,
    "recordsTotal": 115,
    "recordsFiltered": 1,
    "data": [
      {
        "uuid": "8a5bcca3-2d84-4cf9-810f-dfbc7fad7483",
        "name": "wendux",
        "surname": "xyz",
        "username": "tempUser",
        "email": "xyzcyz@gmail.com",
        "phone_number": "7894561230",
        "phone_country_code": "+91",
        "company": "incipinet",
        "activate": 1,
        "email_verified_at": false,
        "image": null,
        "created_at": 1594101416,
        "updated_at": 1596179012,
        "studies_count": 0,
        "image_url": null
      }
    ],
    "queries": [
      {
        "query": "select count(*) as aggregate from (select '1' as `row_count` from `users` where `id` != ? and `users`.`deleted_at` is null limit 1 offset 4) count_row_table",
        "bindings": [
          "1"
        ],
        "time": "0.23"
      },
      {
        "query": "select `users`.*, (select count(*) from `study` where `users`.`id` = `study`.`user_id`) as `studies_count` from `users` where `id` != ? and `users`.`deleted_at` is null limit 1 offset 4",
        "bindings": [
          "1"
        ],
        "time": "0.25"
      }
    ],
    "input": {
      "pageSize": "1",
      "pageNumber": "5",
      "sortField": null,
      "sortOrder": null,
      "filter": {
        "name": null,
        "surname": null,
        "username": null,
        "email": null,
        "company": null,
        "phone_number": null
      }
    }
  }
}
```

3. **Create User** : 
  `POST :  http://php.demo4work.com/mts/backend_api/api/user`

Required Fields :   
a. name   
b. surname   
c.  username   
d. company   
e. phone_number   
f. phone_country_code   
g. email   
h. password
                    
Additional Field : image
                        
4. **Get single User**: You will get uuid fields from above APIs. Use that uuid to fetch single user.   
`POST : http://php.demo4work.com/mts/backend_api/api/user/:uuid`
Response
```
{
  "status": 1,
  "message": "success",
  "message_debug": null,
  "record_count": 1,
  "data": {
    "uuid": "8a5bcca3-2d84-4cf9-810f-dfbc7fad7483",
    "name": "wendux",
    "surname": "xyz",
    "username": "tempUser",
    "email": "xyzcyz@gmail.com",
    "phone_number": "7894561230",
    "phone_country_code": "+91",
    "company": "incipinet",
    "activate": 1,
    "email_verified_at": false,
    "image": null,
    "created_at": 1594101416,
    "updated_at": 1596179012,
    "image_url": null
  }
}
```

5. **Update User** : In update API all fields are optional.

`NOTE : if you are not able to send form data using PUT method then please try method POST and add a field _method:PUT in the form data. Hope it will be a quick fix of the problem.`
  
` PUT : http://php.demo4work.com/mts/backend_api/api/user/{uuid}`

  Editable Fields :   
  a. name   
  b. surname   
  c. username   
  d. company   
  e. phone_number   
  f. phone_country_code   
  g. image   
  h. password   

6. **Delete User** : 

  `DELETE : http://php.demo4work.com/mts/backend_api/api/user/{uuid}`


