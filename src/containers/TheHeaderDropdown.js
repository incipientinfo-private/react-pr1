import React,{Component} from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const imageUrl = localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")).image_url : "";

const  logout = () => {
  localStorage.clear();
  return window.location.href = process.env.PUBLIC_URL+'/login'
}

export default class TheHeaderDropdown extends Component {

  
  constructor(props) {
    super(props);
  }
 
  render(){
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={imageUrl ? imageUrl : "avatars/6.jpg"}
            className="c-avatar-img"
            alt="img"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem>
          <a onClick={logout}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Sign out
          </a>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}
}

